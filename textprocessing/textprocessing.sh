#!/bin/bash 
# Autoren: Aaron Läuchli
# Version: 1.0 
# Datum: 09.11.2021

# Auflistung aller Dateien welche mit .TXT enden
#ls -l | grep "\.txt$"


# Ersetzt alle die im [ ] durch ein u  
#ls -l | sed -e "s/[aeio]/u/g"

# Schreibt alle files die es ausgelesen hat von ls-l in das ls-l.txt file
#ls -l > ls-l.txt

# Gibt die Error Meldung in das File falls die Datei nicht vorhanden ist.
#Cat dateiDieEsNichtGibt.txt 2> error.txt

# /dev/null lässt Fehlermeldung verschwinden
Cat dateiDieEsNichtGibt.txt 2> /dev/null

# Joint zwei Files miteinander
cat list-1 list-2 list-3 | sort | uniq > final.list

sort testfile.txt| uniq -c | sort -nr

man $1 | head -n6 | tail -n1 | cut -b 8-