#!/bin/bash
#
#
# Name: file_diff_u4_1.sh - Unterschied pro Zeile in zwei Dokumenten
#
# -----------------
# SYNOPSIS: file_diff_u4_1.sh File1 File2
#
# Beschreibung: Wird dieses Bashscript ausgeführt, so wird jede Zeile zweier Dokumente miteinander verglichen.
# Alle Unterschiede werden in einem neuen File (diffRows.txt) ausgegeben.
#
# file_diff_u4_1.sh file1 file2
# file1: Dokument1
# file2: Dokument2 (damit wird verglichen)
#
# Autor: Aaron Laeuchli
# Datum: 04.10.2021



echo "------------------------------------"
echo "Dokument 1: $1"
echo "Dokument 2: $2"
echo "------------------------------------"

if [[ -f "$1" && -f "$2" ]]; then                                         #Prüfen, ob beide Dokumente existieren
  echo "Ausgabe der Zeilen:"
  if [[ $(wc -l < "$1") -gt $(wc -l < "$2") ]];                           #Überprüfen, welches Dokument mehr Zeilen hat (-gt = grösser als)
  then
    FileGross=$1                                                          #Variablen deklarieren
    FileKlein=$2
  else
    FileGross=$2
    FileKlein=$1
  fi
  reiheDiff=0
  anzUnterschiedFiles=$(("$(wc -l < "$FileGross")"+1))                   #Anzahl Zeilen aus Dokument lesen, +1 weil die Länge bei 0 beginnt.
  i=1                                                                     #Auf dieser Zeile startet der While
  while [[ $i -le $anzUnterschiedFiles ]]                                #While-Schleife, welche so viel mal wie Anzahl Zeilen durchgeht
  do
    aktuelleReiheGross=$(sed -n ${i}p "$FileGross")                       #Aktuelle Zeile des grossen Dokumentes
    aktuelleReiheKlein=$(sed -n ${i}p "$FileKlein")                       #Aktuelle Zeile des kleinen Dokumentes
    echo "------------------------------------"
    echo "$i: $"
    echo "$i: $aktuelleReiheKlein"
    echo "------------------------------------"
    if [[ "$" != "$aktuelleReiheKlein" ]]; then                           #Überprüfen, ob die aktuelle Zeile differenzen aufweist
      ((reiheDiff=reiheDiff+1))
      echo "Zeile $i nicht gleich"
    fi
    ((i=i+1))
  done
  echo ""
    echo "------------------------------------"
  echo "Insgesamt sind $reiheDiff Zeilen ungleich"
  echo ""
    echo "------------------------------------"
else
  echo "Keine gültige Dokumentangabe"
fi

